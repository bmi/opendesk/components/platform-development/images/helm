# [1.2.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/helm/compare/v1.1.0...v1.2.0) (2024-12-27)


### Features

* **Dockerfile:** Update to alpine v3.21 ([b70c6ba](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/helm/commit/b70c6babe67a7d52eb673df3ba7bd5b266090a35))

# [1.1.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/helm/compare/v1.0.2...v1.1.0) (2024-09-18)


### Features

* **Dockerfile:** Upgrade to helmfile 1.0.0-rc.5 ([623fa23](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/helm/commit/623fa23e747bd79b3714dca7878559c0d417fbc4))

## [1.0.2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/helm/compare/v1.0.1...v1.0.2) (2024-06-09)


### Bug Fixes

* **Dockerfile:** Update images and fix licenses ([c90253b](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/helm/commit/c90253b7a7dee8f72c35285b13c8a7541ca05fce))

## [1.0.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/helm/compare/v1.0.0...v1.0.1) (2024-01-21)


### Bug Fixes

* **Dockerfile:** Fix git version ([3d5f8c0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/helm/commit/3d5f8c05f89c3c931f6ace5478ef992a5003cffe))
* **Dockerfile:** Update helm to 3.14 to speed up pipelines ([9526a47](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/helm/commit/9526a475b4c2f419eb10c9154e538d19a26f14fb))

# 1.0.0 (2023-12-27)


### Bug Fixes

* **ci:** Move to Open CoDE ([e7d031f](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/helm/commit/e7d031fcc736ce55a77355fcaefdef5038feb167))
* **ci:** Update kubectl to 1.29.0 ([cf160fb](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/helm/commit/cf160fba566fb291dbe72f772f7ee20412a95361))
* **Dockerfile:** Add execute permission to yq ([3af4082](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/helm/commit/3af4082f4d9f2888f26dc4f51d6c46c206b0ab23))
* **Dockerfile:** Add kubectl to image ([22bd091](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/helm/commit/22bd091ef1bd2110a7a1ad647759b33ffcf51dc8))
* **Dockerfile:** Fix DL4006 error ([519ead6](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/helm/commit/519ead6e0d6ce368fd5d152bda3a87f5abe02e4b))
* **Dockerfile:** Remove add fixed version for apk packages ([369b19c](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/helm/commit/369b19c3fabf0a2e09dc8686c523566826c44ef3))
* **helm:** Update helm=3.12.3, helmfile=0.157.0, kubectl=1.28.1 ([fb3983c](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/helm/commit/fb3983c7e6a05aaed79a18880ae0e83f324b57d2))
* **helm:** Update helmfile to v0.155.0 ([0be0699](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/helm/commit/0be0699c4cd89a19a228016d03b3505ea4f83e7e))
* **helm:** Update kubectl to v1.27.3-r0 ([41787ef](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/helm/commit/41787ef4cbd8f63249ac046c04852341d906e44c))


### Features

* **Dockerfile:** Add yq to image ([8699bc2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/helm/commit/8699bc2a3fa77c86a1f769d005eaeef9d028dc23))

<!--
SPDX-FileCopyrightText: 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
SPDX-License-Identifier: Apache-2.0
-->
