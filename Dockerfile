# SPDX-FileCopyrightText: 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0
FROM docker.io/alpine:3.21@sha256:21dc6063fd678b478f57c0e13f47560d0ea4eeba26dfc947b2a4f81f686b9f45
LABEL org.opencontainers.image.authors="Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH <hallo@zendis.de>" \
      org.opencontainers.image.documentation=https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/helm/-/blob/main/README.md \
      org.opencontainers.image.source=https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/helm \
      org.opencontainers.image.vendor="Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH <hallo@zendis.de>" \
      org.opencontainers.image.licenses=Apache-2.0 \
      org.opencontainers.image.base.name=docker.io/alpine:3.21 \
      org.opencontainers.image.base.digest=sha256:21dc6063fd678b478f57c0e13f47560d0ea4eeba26dfc947b2a4f81f686b9f45

ARG HELM_VERSION=3.16.4
ARG HELM_DIFF_VERSION=3.9.13
ARG HELMFILE_VERSION=1.0.0-rc.8
ARG KUBECTL_VERSION=1.32.0
ARG YQ_VERION=4.44.6

RUN apk add --update --no-cache \
    wget=1.25.0-r0 \
    git=2.47.1-r0 \
    curl=8.11.1-r0 \
    bash=5.2.37-r0

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

RUN \
 # Kubectl
 wget -q "https://dl.k8s.io/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl" -O /usr/bin/kubectl \
 # Helm
 && wget -q "https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz" -O - | tar -xz linux-amd64/helm \
 && mv linux-amd64/helm /usr/bin/helm \
 && chmod +x /usr/bin/helm \
 # Helmfile
 && wget -q "https://github.com/helmfile/helmfile/releases/download/v${HELMFILE_VERSION}/helmfile_${HELMFILE_VERSION}_linux_amd64.tar.gz" -O - | tar -xz helmfile \
 && mv helmfile /usr/bin/helmfile \
 && chmod +x /usr/bin/helmfile \
 # Helm diff
 && HELM_HOME=$(helm env HELM_PLUGINS) \
 && mkdir -p "${HELM_HOME}/helm-diff/bin" \
 && wget -q "https://github.com/databus23/helm-diff/releases/download/v${HELM_DIFF_VERSION}/helm-diff-linux-amd64.tgz" -O - | tar -xz  \
 && mv diff/bin/diff "${HELM_HOME}/helm-diff/bin" \
 && mv diff/plugin.yaml "${HELM_HOME}/helm-diff" \
 && rm -rf diff \
 # yq
 && wget -q "https://github.com/mikefarah/yq/releases/download/v${YQ_VERION}/yq_linux_amd64" -O /usr/bin/yq \
 && chmod +x /usr/bin/yq
